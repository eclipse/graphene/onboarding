/*-
 * ===============LICENSE_START=======================================================
 * Acumos
 * ===================================================================================
 * Copyright (C) 2017 AT&T Intellectual Property & Tech Mahindra. All rights reserved.
 * ===================================================================================
 * This Acumos software file is distributed by AT&T and Tech Mahindra
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * This file is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ===============LICENSE_END=========================================================
 */

package org.acumos.designstudio.toscagenerator.test;

import com.google.gson.Gson;
import org.acumos.designstudio.toscagenerator.service.ProtobufGeneratorService;
import org.acumos.designstudio.toscagenerator.service.TgifGeneratorService;
import org.acumos.designstudio.toscagenerator.util.ConfigurationProperties;
import org.acumos.designstudio.toscagenerator.vo.Artifact;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * 
 * 
 *
 */
public class ProtobufToJsonConversionV2Test {
	private static final Logger logger = LoggerFactory.getLogger(ProtobufToJsonConversionV2Test.class);

	/**
	 * The test case is used to create protoBuf.json by using a method
	 * createProtoJson which consumes solutionId,version,localProtoFile as
	 * parameters and generates protoBuf data in a string format which is then
	 * stored in protoBuf.json. The file is utilized to create TGIF.json which is
	 * used by ds-composition engine to represent a model.
	 * 
	 * @exception org.acumos.designstudio.toscagenerator.exceptionhandler.ServiceException
	 *                Handles the exception occurred in service class.
	 */
	@Test
	public void getSolutions() throws Exception {
		ConfigurationProperties.init("logs","","","",
				"","","","","");
		String[] protos={"actor", "container", "function", "unified_planning","receiver","sender","aitp"};
		//String[] protos={"aitp"};
		for(String proto: protos) {
			logger.info("***************** start: "+proto);
			ProtobufGeneratorService protoService = new ProtobufGeneratorService();
			File currProto = new File("src/test/resources/" + proto + ".proto");
			assertTrue(currProto.exists());

			String currProto1 = protoService.createProtoJson("1234", "1.0.0", currProto);
			assertNotNull(currProto1);
			logger.debug(proto+" json result :" + currProto1);
			TgifGeneratorService tgifService = new TgifGeneratorService();
			Artifact artifact = tgifService.createTgif("1234", "1.0.0", currProto1, proto, "curr description");
			Gson gson = new Gson();
			String tgifoJsonString = gson.toJson(artifact);
			logger.debug(proto+" tgif: " + tgifoJsonString);
		}
	}

}
