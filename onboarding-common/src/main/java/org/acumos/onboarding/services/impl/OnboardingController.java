/*-
 * ===============LICENSE_START=======================================================
 * Acumos
 * ===================================================================================
 * Copyright (C) 2017 AT&T Intellectual Property & Tech Mahindra. All rights reserved.
 * ===================================================================================
 * This Acumos software file is distributed by AT&T and Tech Mahindra
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * This file is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ===============LICENSE_END=========================================================
 */

package org.acumos.onboarding.services.impl;

import java.io.File;
import java.net.ConnectException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.acumos.cds.CodeNameType;
import org.acumos.cds.domain.MLPCodeNamePair;
import org.acumos.cds.domain.MLPSolution;
import org.acumos.cds.domain.MLPSolutionRevision;
import org.acumos.cds.domain.MLPTask;
import org.acumos.cds.domain.MLPUser;
import org.acumos.cds.transport.AuthorTransport;
import org.acumos.cds.transport.RestPageRequest;
import org.acumos.cds.transport.RestPageResponse;
import org.acumos.onboarding.common.exception.AcumosServiceException;
import org.acumos.onboarding.common.models.OnboardingNotification;
import org.acumos.onboarding.common.models.ServiceResponse;
import org.acumos.onboarding.common.utils.Crediantials;
import org.acumos.onboarding.common.utils.JsonRequest;
import org.acumos.onboarding.common.utils.LogBean;
import org.acumos.onboarding.common.utils.LogThreadLocal;
import org.acumos.onboarding.common.utils.LoggerDelegate;
import org.acumos.onboarding.common.utils.OnboardingConstants;
import org.acumos.onboarding.common.utils.UtilityFunction;
import org.acumos.onboarding.component.docker.preparation.Metadata;
import org.acumos.onboarding.component.docker.preparation.MetadataParser;
import org.acumos.onboarding.logging.OnboardingLogConstants;
import org.acumos.securityverification.domain.Workflow;
import org.acumos.securityverification.utils.SVConstants;
import org.apache.http.conn.HttpHostConnectException;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;



@RestController
@RequestMapping(value = "/v2")
@Api(value = "Operation to to onboard a ML model", tags = "Onboarding Service APIs")
/**
 *
 * @author *****
 *
 */
public class OnboardingController extends CommonOnboarding {
	private static Logger log = LoggerFactory.getLogger(OnboardingController.class);
	LoggerDelegate logger = new LoggerDelegate(log);
	Map<String, String> artifactsDetails = new HashMap<>();
	public static String lOG_DIR_LOC = "/maven/logs/on-boarding/applog";

	@Autowired
	private Environment env;

	public OnboardingController() {
		// Property values are injected after the constructor finishes
	}

	@SuppressWarnings("unchecked")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@ApiOperation(value = "Check User authentication and returns JWT token", response = ServiceResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 500, message = "Something bad happened", response = ServiceResponse.class),
			@ApiResponse(code = 400, message = "Invalid request", response = ServiceResponse.class) })
	@RequestMapping(value = "/auth", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<ServiceResponse> OnboardingWithAuthentication(@RequestBody JsonRequest<Crediantials> cred,
			HttpServletResponse response) throws AcumosServiceException {
		logger.info( "Started User Authentication");
		try {
			Crediantials obj = cred.getBody();

			String user = obj.getUsername();
			String pass = obj.getPassword();

			JSONObject crediantials = new JSONObject();
			crediantials.put("username", user);
			crediantials.put("password", pass);

			JSONObject reqObj = new JSONObject();
			reqObj.put("request_body", crediantials);

			String token = portalClient.loginToAcumos(reqObj);

			if (token != null) {
				// Setting JWT token in header
				response.setHeader("jwtToken", token);
				logger.info( "User Authentication Successful");
				return new ResponseEntity<ServiceResponse>(ServiceResponse.successJWTResponse(token), HttpStatus.OK);
			} else {
				logger.info( "Either Username/Password is invalid.");
				throw new AcumosServiceException(AcumosServiceException.ErrorCode.OBJECT_NOT_FOUND,
						"Either Username/Password is invalid.");
			}

		} catch (AcumosServiceException e) {
			logger.error( e.getMessage(), e);
			return new ResponseEntity<ServiceResponse>(ServiceResponse.errorResponse(e.getErrorCode(), e.getMessage()),
					HttpStatus.UNAUTHORIZED);
		}
	}

	/************************************************
	 * End of Authentication
	 *****************************************************/

	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Onboard model with dockerized model URI", response = ServiceResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Created", response = ServiceResponse.class),
			@ApiResponse(code = 500, message = "Something bad happened", response = ServiceResponse.class),
			@ApiResponse(code = 400, message = "Invalid request", response = ServiceResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized User", response = ServiceResponse.class) })
	@RequestMapping(value = "/advancedModel", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<ServiceResponse> advancedModelOnboard(HttpServletRequest request,
			@RequestPart(required = false) MultipartFile model,
			@RequestPart(required = false) MultipartFile license,
			@RequestPart(required = false) MultipartFile protobuf,
			@RequestHeader(value = "modelname", required = true) String modName,
			@RequestHeader(value = "Authorization", required = false) String authorization,
			@RequestHeader(value = "isCreateMicroservice", required = false) boolean isCreateMicroservice,
			@RequestHeader(value = "dockerfileURL", required = false) String dockerfileURL,
			@RequestHeader(value = "provider", required = false) String provider,
			@RequestHeader(value = "tracking_id", required = false) String trackingID,
			@RequestHeader(value = "Request-ID", required = false) String request_id,
			@RequestHeader(value = "shareUserName", required = false) String shareUserName)
					throws AcumosServiceException {

		OnboardingNotification onboardingStatus = null;

		if (trackingID != null) {
			logger.info( "Tracking ID: " + trackingID);
		} else {
			trackingID = UUID.randomUUID().toString();
			logger.info( "Tracking ID Created: " + trackingID);
		}

		if (request_id != null) {
			logger.info( "Request ID: " + request_id);
		} else {
			request_id = UUID.randomUUID().toString();
			logger.info( "Request ID Created: " + request_id);
		}
		logger.info("dockerFileURL: "+dockerfileURL);

		// code to retrieve the current pom version
		// UtilityFunction.getCurrentVersion();
		onboardingStatus = new OnboardingNotification(env.getProperty("cmndatasvc.cmnDataSvcEndPoinURL"), env.getProperty("cmndatasvc.cmnDataSvcUser"), env.getProperty("cmndatasvc.cmnDataSvcPwd"), request_id);
		onboardingStatus.setRequestId(request_id);
		MDC.put(OnboardingLogConstants.MDCs.REQUEST_ID, request_id);

		String fileName = "AdvancedModelOnboardLog.txt";
		LogBean logBean = new LogBean();
		logBean.setLogPath(lOG_DIR_LOC + File.separator + trackingID);
		logBean.setFileName(fileName);
		LogThreadLocal logThread = new LogThreadLocal();
		logThread.set(logBean);
		// create log file to capture logs as artifact
		UtilityFunction.createLogFile();

		String version = UtilityFunction.getProjectVersion();
		logger.info( "On-boarding version : " + version);

		MLPUser shareUser = null;
		Metadata mData = new Metadata();
		mData.setSolutionName(modName);
		String modelName = null;
		MLPTask task = null;
		long taskId = 0;

		String modelId = UtilityFunction.getGUID();
		File outputFolder = new File("tmp", modelId);
		outputFolder.mkdirs();
		boolean isSuccess = false;
		MLPSolution mlpSolution = null;
		String modelType = null;

		try {

			if((model !=null && !model.isEmpty()) && (dockerfileURL!=null && !dockerfileURL.isEmpty())) {

				logger.error( "Either pass Model File or Docker Uri for this request");
				throw new AcumosServiceException(AcumosServiceException.ErrorCode.INVALID_PARAMETER,
						"Either pass Model File or Docker Uri for this request");
			}

			// 'authorization' represents JWT token here...!
			if (authorization == null) {
				logger.error( "Token Not Available...!");
				throw new AcumosServiceException(AcumosServiceException.ErrorCode.OBJECT_NOT_FOUND,
						"Token Not Available...!");
			}

			if (shareUserName != null) {
				RestPageResponse<MLPUser> user = cdmsClient.findUsersBySearchTerm(shareUserName,
						new RestPageRequest(0, 9));

				List<MLPUser> uList = user.getContent();

				if (uList.isEmpty()) {
					logger.error(
							"User " + shareUserName + " not found: cannot share model; onboarding aborted");
					throw new AcumosServiceException(AcumosServiceException.ErrorCode.OBJECT_NOT_FOUND,
							"User " + shareUserName + " not found: cannot share model; onboarding aborted");
				} else {
					shareUser = uList.get(0);
				}
			}

			modelType = getModelType(model);

			// Call to validate Token .....!
			String ownerId = validate(authorization, provider);

			if (ownerId != null && !ownerId.isEmpty()) {

				logger.info( "Token validation successful");

				MLPSolutionRevision revision;
				File localmodelFile = null;
				File licenseFile = null;

				if (license != null && !license.isEmpty()) {
					String licenseFileName = license.getOriginalFilename();
					String licenseFileExtension = licenseFileName.substring(licenseFileName.lastIndexOf('.'));

					if (!licenseFileExtension.toLowerCase().equalsIgnoreCase(OnboardingConstants.LICENSE_EXTENSION)) {
						logger.info("License file extension of " + licenseFileName + " should be \".json\"");
						return new ResponseEntity<ServiceResponse>(ServiceResponse.errorResponse(
								OnboardingConstants.BAD_REQUEST_CODE,
								OnboardingConstants.LICENSE_FILENAME_ERROR + ". Original File : " + licenseFileName),
								HttpStatus.BAD_REQUEST);
					}

					if (!licenseFileName.toLowerCase().equalsIgnoreCase(OnboardingConstants.LICENSE_FILENAME)) {
						logger.info("Changing License file name = " + licenseFileName + " to \"license.json\"");
						licenseFileName = OnboardingConstants.LICENSE_FILENAME;
					}

					licenseFile = new File(outputFolder, licenseFileName);
					UtilityFunction.copyFile(license.getInputStream(), licenseFile);
				}

				File localProtobufFile = null;

				if(protobuf != null && !protobuf.isEmpty()) {

					String protobufFileName = protobuf.getOriginalFilename();
					String protobufFileExtension = protobufFileName.substring(protobufFileName.lastIndexOf('.'));

					if (!protobufFileExtension.toLowerCase().equalsIgnoreCase(".proto")) {
						logger.info("Protobuf file extension of " + protobufFileName + " should be \".proto\"");
						return new ResponseEntity<ServiceResponse>(ServiceResponse.errorResponse(
								OnboardingConstants.BAD_REQUEST_CODE,
								"Error Occurred: proto File Required . Original File : " + protobufFileName),
								HttpStatus.BAD_REQUEST);
					}

					localProtobufFile = new File(outputFolder, protobuf.getOriginalFilename());
					UtilityFunction.copyFile(protobuf.getInputStream(), localProtobufFile);

				}

				try {

					try {
						// Notify Create solution or get existing solution ID
						// has
						// started
						if (onboardingStatus != null) {

							task = new MLPTask();
							task.setTaskCode("OB");
							task.setStatusCode("ST");
							task.setName("OnBoarding");
							task.setUserId(ownerId);
							task.setCreated(Instant.now());
							task.setModified(Instant.now());
							task.setTrackingId(trackingID);
							onboardingStatus.setTrackingId(trackingID);
							onboardingStatus.setUserId(ownerId);
							task = cdmsClient.createTask(task);

							logger.info( "TaskID: " + task.getTaskId());
							taskId = task.getTaskId();
							onboardingStatus.setTaskId(task.getTaskId());
							onboardingStatus.notifyOnboardingStatus("CreateSolution", "ST", "CreateSolution Started");
						}

						if (modelType.equalsIgnoreCase("interchangedModel")) {

							localmodelFile = new File(outputFolder, model.getOriginalFilename());
							UtilityFunction.copyFile(model.getInputStream(), localmodelFile);
						}

						logger.info( "Set the owner ID and Model Name");
						mData.setOwnerId(ownerId);
						mData.setModelName(modName);

						List<MLPSolution> solList = getExistingSolution(mData);
						boolean isListEmpty = solList.isEmpty();

						if (isListEmpty) {
							mlpSolution = createSolution(mData, onboardingStatus);
							mData.setSolutionId(mlpSolution.getSolutionId());
							logger.info(
									"New solution created Successfully " + mlpSolution.getSolutionId());
						} else {
							logger.info(
									"Existing solution found for model name " + solList.get(0).getName());
							mlpSolution = solList.get(0);
							mData.setSolutionId(mlpSolution.getSolutionId());
						}

						revision = createSolutionRevision(mData, localProtobufFile);
						modelName = mData.getModelName();

						// Solution id creation completed
						// Notify Creation of solution ID is successful
						if (onboardingStatus != null) {
							// set solution Id
							if (mlpSolution.getSolutionId() != null) {
								onboardingStatus.setSolutionId(mlpSolution.getSolutionId());
							}
							// set revision id
							if (mData.getRevisionId() != null) {
								onboardingStatus.setRevisionId(mData.getRevisionId());
							}
							// notify
							onboardingStatus.notifyOnboardingStatus("CreateSolution", "SU",
									"CreateSolution Successful");
						}
					} catch (AcumosServiceException e) {
						MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE, OnboardingLogConstants.ResponseStatus.ERROR.name());
						MDC.put(OnboardingLogConstants.MDCs.RESPONSE_DESCRIPTION, HttpStatus.INTERNAL_SERVER_ERROR.toString());
						MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, HttpStatus.INTERNAL_SERVER_ERROR.toString());
						HttpStatus httpCode = HttpStatus.INTERNAL_SERVER_ERROR;
						logger.error( e.getErrorCode() + "  " + e.getMessage());
						if (e.getErrorCode().equalsIgnoreCase(OnboardingConstants.INVALID_PARAMETER)) {
							httpCode = HttpStatus.BAD_REQUEST;
						}
						// Create Solution failed. Notify
						if (onboardingStatus != null) {
							// notify
							onboardingStatus.notifyOnboardingStatus("CreateSolution", "FA", e.getMessage());
						}
						return new ResponseEntity<ServiceResponse>(
								ServiceResponse.errorResponse(e.getErrorCode(), e.getMessage(), modelName), httpCode);
					} catch (Exception e) {
						MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE, OnboardingLogConstants.ResponseStatus.ERROR.name());
						MDC.put(OnboardingLogConstants.MDCs.RESPONSE_DESCRIPTION, HttpStatus.INTERNAL_SERVER_ERROR.toString());
						MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, HttpStatus.INTERNAL_SERVER_ERROR.toString());
						logger.error( e.getMessage());
						// Create Solution failed. Notify
						if (onboardingStatus != null) {
							// notify
							onboardingStatus.notifyOnboardingStatus("CreateSolution", "FA", e.getMessage());
						}
						if (e instanceof AcumosServiceException) {
							return new ResponseEntity<ServiceResponse>(
									ServiceResponse.errorResponse(((AcumosServiceException) e).getErrorCode(),
											e.getMessage(), modelName),
									HttpStatus.INTERNAL_SERVER_ERROR);
						} else {
							return new ResponseEntity<ServiceResponse>(
									ServiceResponse.errorResponse(AcumosServiceException.ErrorCode.UNKNOWN.name(),
											e.getMessage(), modelName),
									HttpStatus.INTERNAL_SERVER_ERROR);
						}
					}

					String dockerImageUri = null;
					logger.info("model type="+modelType);
					artifactsDetails = getArtifactsDetails();

					if (dockerfileURL != null) {
						addArtifact(mData, dockerfileURL, getArtifactTypeCode("Docker Image"), null);
					} else if (modelType.equalsIgnoreCase("interchangedModel")) {
						addArtifact(mData, localmodelFile, getArtifactTypeCode("Model Image"), mData.getModelName(),
								onboardingStatus);
					} else if(modelType.equalsIgnoreCase("other")) {
						//Need to add modelType.equalsIgnoreCase("dockerImage")
						dockerImageUri = imagetagProxyPrefix+ File.separator + modelName +":" +mData.getVersion();
						logger.info( "dockerImageUri: " + dockerImageUri);
						addArtifact(mData, dockerImageUri, getArtifactTypeCode("Docker Image"),
								onboardingStatus);
					}

					if (license != null && !license.isEmpty()) {
						addArtifact(mData, licenseFile, getArtifactTypeCode(OnboardingConstants.ARTIFACT_TYPE_LICENSE_LOG),
								"license", onboardingStatus);
					}

					if(protobuf != null && !protobuf.isEmpty()) {
						addArtifact(mData, localProtobufFile, getArtifactTypeCode("Model Image"), mData.getModelName(),
								onboardingStatus);
					}

					// Notify TOSCA generation started
					if (onboardingStatus != null) {
						onboardingStatus.notifyOnboardingStatus("CreateTOSCA", "ST", "TOSCA Generation Started");
					}

					if(protobuf != null && !protobuf.isEmpty()) {
						generateTOSCA(localProtobufFile, null, mData, onboardingStatus);
					}

					// Notify TOSCA generation successful
					if (onboardingStatus != null) {
						onboardingStatus.notifyOnboardingStatus("CreateTOSCA", "SU", "TOSCA Generation Successful");
					}

					logger.info( "isCreateMicroservice: " + isCreateMicroservice);

					ResponseEntity<ServiceResponse> response = null;

					// call microservice
					if (isCreateMicroservice) {
						logger.info( "Before microservice call Parameters : SolutionId "
								+ mlpSolution.getSolutionId() + " and RevisionId " + revision.getRevisionId());
						try {
							response = microserviceClient.generateMicroservice(
									mlpSolution.getSolutionId(), revision.getRevisionId(), provider, authorization,
									trackingID, mData.getModelName(), null, request_id);
							if (response.getStatusCodeValue() == 200 || response.getStatusCodeValue() == 201) {
								isSuccess = true;
							}
							taskId = response.getBody().getTaskId();
						} catch (Exception e) {
							if (e instanceof HttpHostConnectException || e.getCause() instanceof ConnectException) {
								if (onboardingStatus != null) {
									onboardingStatus.notifyOnboardingStatus("Dockerize", "FA", e.getMessage());
								}
								logger.info( "Dockerize Failed due to connectException: " + e.getMessage());
								throw new ConnectException("ConnectException occured while invoking microservice API " + e.getMessage());
							}

							logger.error(
									"Exception occured while invoking microservice API " + e);
							throw e;
						}
					} else {
						isSuccess = true;
					}

					// Model Sharing
					if (isSuccess && (shareUserName != null) && revision.getRevisionId() != null) {
						try {
							AuthorTransport author = new AuthorTransport(shareUserName, shareUser.getEmail());
							AuthorTransport authors[] = new AuthorTransport[1];
							logger.info(
									"Author Name " + author.getName() + " and Email " + author.getContact());
							authors[0] = author;
							revision.setAuthors(authors);
							cdmsClient.updateSolutionRevision(revision);
							logger.info(
									"Model Shared Successfully with " + shareUserName);
							MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE,
									HttpStatus.CREATED.toString());
						} catch (Exception e) {
							isSuccess = false;
							logger.error( " Failed to share Model", e);
							throw e;
						}
					}

					ResponseEntity<ServiceResponse> res = new ResponseEntity<ServiceResponse>(
							ServiceResponse.successResponse(mlpSolution, taskId, trackingID,dockerImageUri), HttpStatus.CREATED);
					logger.info(
							"Onboarding is successful for model name: " + mlpSolution.getName() + ", SolutionID: "
									+ mlpSolution.getSolutionId() + ", Status Code: " + res.getStatusCode());
					return res;
				} finally {

					try {
						UtilityFunction.deleteDirectory(outputFolder);

						if (isSuccess == false) {
							task.setSolutionId(mData.getSolutionId());
							task.setRevisionId(mData.getRevisionId());
							task.setStatusCode("FA");
							logger.info("MLP task updating with the values =" + task.toString());
							cdmsClient.updateTask(task);
							logger.info("Onboarding Failed, Reverting failed solutions and artifacts.");
							if (metadataParser != null && mData != null) {
								revertbackOnboarding(metadataParser.getMetadata(), mlpSolution.getSolutionId());
							}
						}

						if (isSuccess == true) {
							MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE, OnboardingLogConstants.ResponseStatus.COMPLETED.name());
							MDC.put(OnboardingLogConstants.MDCs.RESPONSE_DESCRIPTION, "Advanced Model Onboarding Completed");
							MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, HttpStatus.CREATED.toString());

							task.setSolutionId(mData.getSolutionId());
							task.setRevisionId(mData.getRevisionId());
							task.setStatusCode("SU");
							logger.info("MLP task updating with the values =" + task.toString());
							cdmsClient.updateTask(task);
						}

						// push docker build log into nexus

						File file = new java.io.File(
								lOG_DIR_LOC + File.separator + trackingID + File.separator + fileName);
						logger.debug( "Log file length " + file.length(), file.getPath(),
								fileName);
						if (mData != null) {
							logger.info("Adding of log artifacts into nexus started " + fileName);

							// String nexusArtifactID = "onboardingLog_"+trackingID;
							String nexusArtifactID = "OnboardingLog";

							addArtifact(mData, file, getArtifactTypeCode(OnboardingConstants.ARTIFACT_TYPE_LOG),
									nexusArtifactID, onboardingStatus);
							MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE,
									OnboardingLogConstants.ResponseStatus.COMPLETED.name());
							logger.info( "Artifacts log pushed to nexus successfully " + fileName);
						}

						// delete log file
						UtilityFunction.deleteDirectory(file);
						logThread.unset();
						mData = null;
					} catch (AcumosServiceException e) {
						MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE, OnboardingLogConstants.ResponseStatus.ERROR.name());
						MDC.put(OnboardingLogConstants.MDCs.RESPONSE_DESCRIPTION, HttpStatus.INTERNAL_SERVER_ERROR.toString());
						MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, HttpStatus.INTERNAL_SERVER_ERROR.toString());
						mData = null;
						logger.error( "RevertbackOnboarding Failed");
						HttpStatus httpCode = HttpStatus.INTERNAL_SERVER_ERROR;
						return new ResponseEntity<ServiceResponse>(
								ServiceResponse.errorResponse(e.getErrorCode(), e.getMessage(), modelName), httpCode);
					}
				}
			} else {
				try {
					MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE, OnboardingLogConstants.ResponseStatus.ERROR.name());
					MDC.put(OnboardingLogConstants.MDCs.RESPONSE_DESCRIPTION, "Either Username/Password is invalid.");
					MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, HttpStatus.UNAUTHORIZED.toString());
					logger.error( "Either Username/Password is invalid.");
					throw new AcumosServiceException(AcumosServiceException.ErrorCode.INVALID_TOKEN,
							"Either Username/Password is invalid.");
				} catch (AcumosServiceException e) {
					return new ResponseEntity<ServiceResponse>(
							ServiceResponse.errorResponse(e.getErrorCode(), e.getMessage()), HttpStatus.UNAUTHORIZED);
				}
			}

		} catch (AcumosServiceException e) {

			HttpStatus httpCode = HttpStatus.INTERNAL_SERVER_ERROR;
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE, OnboardingLogConstants.ResponseStatus.ERROR.name());
			logger.error( e.getErrorCode() + "  " + e.getMessage());
			if (e.getErrorCode().equalsIgnoreCase(OnboardingConstants.INVALID_PARAMETER)) {
				httpCode = HttpStatus.BAD_REQUEST;
			}
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_DESCRIPTION,  httpCode.toString());
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, httpCode.toString());
			return new ResponseEntity<ServiceResponse>(
					ServiceResponse.errorResponse(e.getErrorCode(), e.getMessage(), modelName), httpCode);
		} catch (HttpClientErrorException e) {
			// Handling #401 and 400(BAD_REQUEST) is added as CDS throws 400 if apitoken is
			// invalid.
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE, OnboardingLogConstants.ResponseStatus.ERROR.name());
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_DESCRIPTION, e.getMessage());
			if (HttpStatus.UNAUTHORIZED == e.getStatusCode() || HttpStatus.BAD_REQUEST == e.getStatusCode()) {
				logger.info(
						"Unauthorized User - Either Username/Password is invalid.");
				MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, e.getStatusCode().toString());
				return new ResponseEntity<ServiceResponse>(
						ServiceResponse.errorResponse("" + HttpStatus.UNAUTHORIZED, "Unauthorized User", modelName),
						HttpStatus.UNAUTHORIZED);
			} else {
				logger.error( e.getMessage(), e);
				MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, e.getStatusCode().toString());
				return new ResponseEntity<ServiceResponse>(
						ServiceResponse.errorResponse("" + e.getStatusCode(), e.getMessage(), modelName),
						e.getStatusCode());
			}
		} catch (Exception e) {
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE, OnboardingLogConstants.ResponseStatus.ERROR.name());
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_DESCRIPTION, HttpStatus.INTERNAL_SERVER_ERROR.toString());
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, HttpStatus.INTERNAL_SERVER_ERROR.toString());
			logger.error( "onboardModel Failed Exception " + e.getMessage(), e);
			if (e instanceof AcumosServiceException) {
				return new ResponseEntity<ServiceResponse>(ServiceResponse
						.errorResponse(((AcumosServiceException) e).getErrorCode(), e.getMessage(), modelName),
						HttpStatus.INTERNAL_SERVER_ERROR);
			} else {
				return new ResponseEntity<ServiceResponse>(ServiceResponse
						.errorResponse(AcumosServiceException.ErrorCode.UNKNOWN.name(), e.getMessage(), modelName),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Onboard pipeline", response = ServiceResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Created", response = ServiceResponse.class),
			@ApiResponse(code = 500, message = "Something bad happened", response = ServiceResponse.class),
			@ApiResponse(code = 400, message = "Invalid request", response = ServiceResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized User", response = ServiceResponse.class) })
	@RequestMapping(value = "/onboardPipeline", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<ServiceResponse> onboardPipeline(HttpServletRequest request,
			@RequestPart(required = true) MultipartFile blueprint,
			@RequestPart(required = true) MultipartFile cdump,
			@RequestPart(required = false) MultipartFile license,
			@RequestHeader(value = "modelname", required = true) String modName,
			@RequestHeader(value = "Authorization", required = false) String authorization,
			@RequestHeader(value = "provider", required = false) String provider,
			@RequestHeader(value = "tracking_id", required = false) String trackingID,
			@RequestHeader(value = "Request-ID", required = false) String request_id)
			throws AcumosServiceException {

		OnboardingNotification onboardingStatus = null;

		if (trackingID != null) {
			logger.info( "Tracking ID: " + trackingID);
		} else {
			trackingID = UUID.randomUUID().toString();
			logger.info( "Tracking ID Created: " + trackingID);
		}

		if (request_id != null) {
			logger.info( "Request ID: " + request_id);
		} else {
			request_id = UUID.randomUUID().toString();
			logger.info( "Request ID Created: " + request_id);
		}

		onboardingStatus = new OnboardingNotification(env.getProperty("cmndatasvc.cmnDataSvcEndPoinURL"), env.getProperty("cmndatasvc.cmnDataSvcUser"), env.getProperty("cmndatasvc.cmnDataSvcPwd"), request_id);
		onboardingStatus.setRequestId(request_id);
		MDC.put(OnboardingLogConstants.MDCs.REQUEST_ID, request_id);

		String fileName = "OnboardPipeline.txt";
		LogBean logBean = new LogBean();
		logBean.setLogPath(lOG_DIR_LOC + File.separator + trackingID);
		logBean.setFileName(fileName);
		LogThreadLocal logThread = new LogThreadLocal();
		logThread.set(logBean);
		// create log file to capture logs as artifact
		UtilityFunction.createLogFile();

		String version = UtilityFunction.getProjectVersion();
		logger.info( "On-boarding version : " + version);

		MLPUser shareUser = null;
		Metadata mData = new Metadata();
		mData.setSolutionName(modName);

		String modelId = UtilityFunction.getGUID();
		File outputFolder = new File("tmp", modelId);
		outputFolder.mkdirs();
		boolean isSuccess = false;
		MLPSolution mlpSolution = null;

		try {


			// 'authorization' represents JWT token here...!
			if (authorization == null) {
				logger.error( "Token Not Available...!");
				throw new AcumosServiceException(AcumosServiceException.ErrorCode.OBJECT_NOT_FOUND,
						"Token Not Available...!");
			}
			// Call to validate Token .....!
			String ownerId = validate(authorization, provider);

			if (ownerId != null && !ownerId.isEmpty()) {

				logger.info( "Token validation successful");

				logger.info( "Set the owner ID and Model Name");
				mData.setOwnerId(ownerId);
				mData.setModelName(modName);
				mData.setToolkit("Composite Solution");

				List<MLPSolution> solList = getExistingSolution(mData);
				if (solList.isEmpty()) {
					mlpSolution = createSolution(mData, onboardingStatus);
					mData.setSolutionId(mlpSolution.getSolutionId());
					logger.info(
							"New solution created Successfully " + mlpSolution.getSolutionId());
				} else {
					logger.info("pipeline name already exists: " + modName);
					return new ResponseEntity<ServiceResponse>(ServiceResponse.errorResponse(
							OnboardingConstants.BAD_REQUEST_CODE,
							"Error Occurred: pipeline name already exists: " + modName),
							HttpStatus.BAD_REQUEST);
				}

				MLPSolutionRevision revision = createSolutionRevision(mData);
				String upperSolutionId= mData.getSolutionId().toUpperCase();

				File licenseFile = null;

				if (license != null && !license.isEmpty()) {
					String licenseFileName = license.getOriginalFilename();
					String licenseFileExtension = licenseFileName.substring(licenseFileName.lastIndexOf('.'));

					if (!licenseFileExtension.toLowerCase().equalsIgnoreCase(OnboardingConstants.LICENSE_EXTENSION)) {
						logger.info("License file extension of " + licenseFileName + " should be \".json\"");
						return new ResponseEntity<ServiceResponse>(ServiceResponse.errorResponse(
								OnboardingConstants.BAD_REQUEST_CODE,
								OnboardingConstants.LICENSE_FILENAME_ERROR + ". Original File : " + licenseFileName),
								HttpStatus.BAD_REQUEST);
					}

					if (!licenseFileName.toLowerCase().equalsIgnoreCase(OnboardingConstants.LICENSE_FILENAME)) {
						logger.info("Changing License file name = " + licenseFileName + " to \"license.json\"");
						licenseFileName = OnboardingConstants.LICENSE_FILENAME;
					}

					licenseFile = new File(outputFolder, licenseFileName);
					UtilityFunction.copyFile(license.getInputStream(), licenseFile);
				}

				File localBlueprintFile = null;

				if(blueprint != null && !blueprint.isEmpty()) {

					String blueprintFileName = blueprint.getOriginalFilename();
					String blueprintFileExtension = blueprintFileName.substring(blueprintFileName.lastIndexOf('.'));

					if (!blueprintFileExtension.toLowerCase().equalsIgnoreCase(".json")) {
						logger.info("Blueprint file extension of " + blueprintFileName + " should be \".json\"");
						return new ResponseEntity<ServiceResponse>(ServiceResponse.errorResponse(
								OnboardingConstants.BAD_REQUEST_CODE,
								"Error Occurred: blueprint File Required . Original File : " + blueprintFileName),
								HttpStatus.BAD_REQUEST);
					}

					localBlueprintFile = new File(outputFolder, "BLUEPRINT-"+upperSolutionId+".json");
					UtilityFunction.copyFile(blueprint.getInputStream(), localBlueprintFile);
				}

				File localCdumpFile = null;

				if(cdump != null && !cdump.isEmpty()) {

					String cdumpFileName = cdump.getOriginalFilename();
					String cdumpFileExtension = cdumpFileName.substring(cdumpFileName.lastIndexOf('.'));

					if (!cdumpFileExtension.toLowerCase().equalsIgnoreCase(".json")) {
						logger.info("Cdump file extension of " + cdumpFileName + " should be \".json\"");
						return new ResponseEntity<ServiceResponse>(ServiceResponse.errorResponse(
								OnboardingConstants.BAD_REQUEST_CODE,
								"Error Occurred: Cdump File Required . Original File : " + cdumpFileName),
								HttpStatus.BAD_REQUEST);
					}

					String cdumpString = new String(cdump.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
					String replacement="\"solutionId\":\""+mData.getSolutionId()+"\"";
					String cdumpCorrected=cdumpString.replaceAll("\"solutionId\":\".+?\"", replacement);

					localCdumpFile = new File(outputFolder, "CDUMP-"+upperSolutionId+".json");
					Files.writeString(localCdumpFile.toPath(), cdumpCorrected, StandardOpenOption.CREATE_NEW);
				}


				try {
					artifactsDetails = getArtifactsDetails();
					// from cmn-data-svc application.properties:
					// artifact-type.BP=Blueprint File
					// artifact-type.CD=Cdump File
					// artifact-type.LI=License

					if (license != null && !license.isEmpty()) {
						addArtifact(mData,
									licenseFile,
									getArtifactTypeCode(OnboardingConstants.ARTIFACT_TYPE_LICENSE_LOG),
									"license",
									onboardingStatus);
					}

					if(blueprint != null && !blueprint.isEmpty()) {
						addArtifact(mData,
									localBlueprintFile,
									"BP",
									localBlueprintFile.getName(),
									onboardingStatus);
					}

					if(cdump != null && !cdump.isEmpty()) {
						addArtifact(mData,
									localCdumpFile,
									"CD",
									localCdumpFile.getName(),
									onboardingStatus);
					}

					isSuccess = true;
					ResponseEntity<ServiceResponse> res = new ResponseEntity<ServiceResponse>(
							ServiceResponse.successResponse(mlpSolution, 0, trackingID,"none"), HttpStatus.CREATED);
					logger.info(
							"Onboarding is successful for pipeline name: " + mlpSolution.getName() + ", SolutionID: "
									+ mlpSolution.getSolutionId() + ", Status Code: " + res.getStatusCode());
					return res;
				} finally {

					try {
						UtilityFunction.deleteDirectory(outputFolder);

						if (isSuccess == false) {
							logger.info("Onboarding pipeline failed, reverting failed solutions and artifacts.");
							if (metadataParser != null && mData != null) {
								revertbackOnboarding(metadataParser.getMetadata(), mlpSolution.getSolutionId());
							}
						}

						if (isSuccess == true) {
							MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE, OnboardingLogConstants.ResponseStatus.COMPLETED.name());
							MDC.put(OnboardingLogConstants.MDCs.RESPONSE_DESCRIPTION, "Onboarding pipeline completed");
							MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, HttpStatus.CREATED.toString());

						}

						// push docker build log into nexus

						File file = new java.io.File(
								lOG_DIR_LOC + File.separator + trackingID + File.separator + fileName);
						logger.debug( "Log file length " + file.length(), file.getPath(), fileName);
						if (mData != null) {
							logger.info(
									"Adding of log artifacts into nexus started " + fileName);

							// String nexusArtifactID = "onboardingLog_"+trackingID;
							String nexusArtifactID = "OnboardingLog";

							addArtifact(mData, file, getArtifactTypeCode(OnboardingConstants.ARTIFACT_TYPE_LOG),
									nexusArtifactID, onboardingStatus);
							MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE,
									OnboardingLogConstants.ResponseStatus.COMPLETED.name());
							logger.info( "Artifacts log pushed to nexus successfully "+fileName);
						}

						// delete log file
						UtilityFunction.deleteDirectory(file);
						logThread.unset();
						mData = null;
					} catch (AcumosServiceException e) {
						MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE, OnboardingLogConstants.ResponseStatus.ERROR.name());
						MDC.put(OnboardingLogConstants.MDCs.RESPONSE_DESCRIPTION, HttpStatus.INTERNAL_SERVER_ERROR.toString());
						MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, HttpStatus.INTERNAL_SERVER_ERROR.toString());
						mData = null;
						logger.error( "RevertbackOnboarding Failed");
						HttpStatus httpCode = HttpStatus.INTERNAL_SERVER_ERROR;
						return new ResponseEntity<ServiceResponse>(
								ServiceResponse.errorResponse(e.getErrorCode(), e.getMessage(), modName), httpCode);
					}
				}
			} else {
				try {
					MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE, OnboardingLogConstants.ResponseStatus.ERROR.name());
					MDC.put(OnboardingLogConstants.MDCs.RESPONSE_DESCRIPTION, "Either Username/Password is invalid.");
					MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, HttpStatus.UNAUTHORIZED.toString());
					logger.error( "Either Username/Password is invalid.");
					throw new AcumosServiceException(AcumosServiceException.ErrorCode.INVALID_TOKEN,
							"Either Username/Password is invalid.");
				} catch (AcumosServiceException e) {
					return new ResponseEntity<ServiceResponse>(
							ServiceResponse.errorResponse(e.getErrorCode(), e.getMessage()), HttpStatus.UNAUTHORIZED);
				}
			}

		} catch (AcumosServiceException e) {

			HttpStatus httpCode = HttpStatus.INTERNAL_SERVER_ERROR;
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE, OnboardingLogConstants.ResponseStatus.ERROR.name());
			logger.error( e.getErrorCode() + "  " + e.getMessage());
			if (e.getErrorCode().equalsIgnoreCase(OnboardingConstants.INVALID_PARAMETER)) {
				httpCode = HttpStatus.BAD_REQUEST;
			}
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_DESCRIPTION,  httpCode.toString());
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, httpCode.toString());
			return new ResponseEntity<ServiceResponse>(
					ServiceResponse.errorResponse(e.getErrorCode(), e.getMessage(), modName), httpCode);
		} catch (HttpClientErrorException e) {
			// Handling #401 and 400(BAD_REQUEST) is added as CDS throws 400 if apitoken is
			// invalid.
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE, OnboardingLogConstants.ResponseStatus.ERROR.name());
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_DESCRIPTION, e.getMessage());
			if (HttpStatus.UNAUTHORIZED == e.getStatusCode() || HttpStatus.BAD_REQUEST == e.getStatusCode()) {
				logger.info(
						"Unauthorized User - Either Username/Password is invalid.");
				MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, e.getStatusCode().toString());
				return new ResponseEntity<ServiceResponse>(
						ServiceResponse.errorResponse("" + HttpStatus.UNAUTHORIZED, "Unauthorized User", modName),
						HttpStatus.UNAUTHORIZED);
			} else {
				logger.error( e.getMessage(), e);
				MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, e.getStatusCode().toString());
				return new ResponseEntity<ServiceResponse>(
						ServiceResponse.errorResponse("" + e.getStatusCode(), e.getMessage(), modName),
						e.getStatusCode());
			}
		} catch (Exception e) {
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_STATUS_CODE, OnboardingLogConstants.ResponseStatus.ERROR.name());
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_DESCRIPTION, HttpStatus.INTERNAL_SERVER_ERROR.toString());
			MDC.put(OnboardingLogConstants.MDCs.RESPONSE_CODE, HttpStatus.INTERNAL_SERVER_ERROR.toString());
			logger.error( "onboardModel Failed Exception " + e.getMessage(), e);
			if (e instanceof AcumosServiceException) {
				return new ResponseEntity<ServiceResponse>(ServiceResponse
						.errorResponse(((AcumosServiceException) e).getErrorCode(), e.getMessage(), modName),
						HttpStatus.INTERNAL_SERVER_ERROR);
			} else {
				return new ResponseEntity<ServiceResponse>(ServiceResponse
						.errorResponse(AcumosServiceException.ErrorCode.UNKNOWN.name(), e.getMessage(), modName),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	
	
	/**
	 *  This method returns model type
	 * @param model
	 * @return model type
	 */
	private String getModelType(MultipartFile model) {

		String modelType;
		if (model != null && !model.isEmpty()) {

			String fileExt = getExtensionOfFile(model.getOriginalFilename());
			if (fileExt.equalsIgnoreCase("onnx") || fileExt.equalsIgnoreCase("pfa")) {
				modelType = "interchangedModel";
				logger.info( "ModelType is " + modelType);
			} else if (fileExt.equalsIgnoreCase("tar")) {
				modelType = "dockerImage";
				logger.info( "ModelType is " + modelType);
			} else {
				modelType = "other";
				logger.info( "ModelType is " + modelType);
			}

		} else {
			modelType = "other";
			logger.info( "ModelType is " + modelType);
		}

		return modelType;

	}

	private Map<String, String> getArtifactsDetails() {
		List<MLPCodeNamePair> typeCodeList = cdmsClient.getCodeNamePairs(CodeNameType.ARTIFACT_TYPE);
		Map<String, String> artifactsDetails = new HashMap<>();
		if (!typeCodeList.isEmpty()) {
			for (MLPCodeNamePair codeNamePair : typeCodeList) {
				artifactsDetails.put(codeNamePair.getName(), codeNamePair.getCode());
			}
		}
		return artifactsDetails;
	}

	private String getArtifactTypeCode(String artifactTypeName) {
		String typeCode = artifactsDetails.get(artifactTypeName);
		return typeCode;
	}

	@Override
	public String getCmnDataSvcEndPoinURL() {
		return cmnDataSvcEndPoinURL;
	}

	@Override
	public String getCmnDataSvcUser() {
		return cmnDataSvcUser;
	}

	@Override
	public String getCmnDataSvcPwd() {
		return cmnDataSvcPwd;
	}

}
